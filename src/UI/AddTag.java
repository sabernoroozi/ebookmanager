package UI;


//import book.BookTagDao;
//import book.Tag;
//import book.TagDao;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.Dao.BookTagDao;
import model.Dao.TagDao;
import model.Manager;
import model.Tag;

import javax.swing.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * AddTag class manages what should be done when user wants to add a new Tag to their application
 *
 * @author  Houra Keshavarz
 * @version 1.0
 * @since   2019-08-16
 */

public class AddTag {
    Connection connection = new Manager().getConnection();
    TagDao tagDao = new TagDao(connection);
    AnchorPane addTagPane = new AnchorPane();
    Scene scene = new Scene(addTagPane, 400,400);
    Stage stage = new Stage();
    GridPane tagGridPane = new GridPane();
    VBox vBox = new VBox();
    Integer counter = 1;
    List<Tag> tagList= new ArrayList<>();

    public AddTag() {
        initialize();
    }
    public  void initialize(){
        stage.setScene(scene);
        stage.show();
        TextField addTagText = new TextField();
        ColorPicker tagColor = new ColorPicker(Color.RED);
        addTagText.setLayoutX(4);
        addTagText.setLayoutY(14);
        tagColor.setLayoutX(155);
        tagColor.setLayoutY(14);

        tagGridPane.setLayoutX(4);
        tagGridPane.setLayoutY(45);
        tagGridPane.setPrefHeight(300);
        tagGridPane.setPrefWidth(300);
        for(Tag tag:tagDao.getAllTags()){
            Label label = new Label("#"+tag.getLabel()+"  ");
            label.setTextFill(tag.getColor());
            handleRightClick(label, tag);
            tagGridPane.add(label, counter % 7 + 1, (counter / 7) + 1);
            counter++;
        }

        vBox.setLayoutX(14);
        vBox.setLayoutY(40);

        handleNewTag(addTagText, tagColor);
        addTagPane.getChildren().addAll(addTagText, tagColor, tagGridPane);
    }
    public void handleNewTag(TextField textField, ColorPicker colorPicker){
        textField.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent ke)
            {
                if (ke.getCode().equals(KeyCode.ENTER))
                {
                    if(textField.getText()!="") {
                        System.out.println("Tags: "+ tagDao.getAllTags());
                        Tag tag = new Tag(0, textField.getText(), colorPicker.getValue());
                        if(tagDao.getAllTags().stream().filter(Tag -> Tag.getLabel().equals
                                ( tag.getLabel())).findAny().isPresent()==false) {
                            tagDao.addTag(tag);
                            BookTagDao bookTagDao = new BookTagDao(connection);
                            Label tagLabel = new Label("#" + tag.getLabel() + "  ");
                            tagLabel.setTextFill(tag.getColor());
                            tagGridPane.add(tagLabel, counter % 7 + 1, (counter / 7) + 1);
                            tagList.add(tag);
                            handleRightClick(tagLabel, tag);
                            counter++;
                        }else{
                            JOptionPane.showMessageDialog(null,"This Tag Already Exists");
                        }
                        textField.deleteText(0, textField.getText().length());
                    }
                }
            }
        });
    }
    public void handleRightClick(Label label,Tag tag) {
        ContextMenu cm = new ContextMenu();
        MenuItem delete = new MenuItem("delete");
        cm.getItems().add(delete);
        label.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent event) {
                cm.show(label, event.getScreenX(), event.getScreenY());
            }
        });
        handleDeleteTag(delete, label,tag);
    }
    private void handleDeleteTag(MenuItem mi, Label l,Tag tag) {
        mi.setOnAction(event -> {
                    l.setText("");
                    tagList.remove(tag);
                    tagDao.deleteTag(tag);
                }
        );
    }
}
