package UI;



import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.Dao.TagDao;
import model.Manager;
import model.Tag;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * AddTagToBook class manages assigning tags to a selected book from
 * existing tags in tag database table
 *
 * @author  Houra Keshavarz
 * @version 1.0
 * @since   2019-08-16
 */
public class AddTagToBook {
    ComboBox comboBox;
    AnchorPane anchorPane;
    GridPane tagsOfBookGridPane;
    static Integer counter;
    TagDao tagDao = new TagDao(new Manager().getConnection());
    static List<Tag> tagsOfBookList = new ArrayList<>();

    public AddTagToBook(ComboBox comboBox, GridPane gridPane, List<Tag> tags) {
        counter = tags.isEmpty()?1:tags.size()+1;
        this.comboBox = comboBox;
        tagsOfBookList=tags;
        tagsOfBookGridPane=gridPane;
        tagsOfBookGridPane.setLayoutX(14);
        tagsOfBookGridPane.setLayoutY(55);
        selectTag();
    }

    private void selectTag() {
        Label l = (Label) comboBox.getSelectionModel().getSelectedItem();
        Label tagLabel = new Label("#" + l.getText()+"  ");
        Color tagColor =tagDao.getAllTags().stream().filter(Tag->
                Tag.getLabel().equals(l.getText())).findAny().get().getColor();
        tagLabel.setTextFill(tagColor);
        Tag tag = new Tag(0, l.getText(), tagColor);
        if(tagsOfBookList.stream().filter(Tag -> Tag.getLabel().equals( tag.getLabel())).findAny().isPresent()==false) {
            tagsOfBookList.add(tag);
            tagsOfBookGridPane.add(tagLabel, counter % 7 + 1, (counter / 7) + 1);
        }else{
            JOptionPane.showMessageDialog(null,"Already Added!");
        }
        counter++;
        handleRightClick(tagLabel,tag);
    }

    public void handleRightClick(Label label,Tag tag) {
        ContextMenu cm = new ContextMenu();
        javafx.scene.control.MenuItem delete = new javafx.scene.control.MenuItem("delete");
        cm.getItems().add(delete);
        label.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent event) {
                cm.show(label, event.getScreenX(), event.getScreenY());
            }
        });
        handleDeleteTag(delete, label,tag);
    }
    private void handleDeleteTag(MenuItem mi, Label l, Tag tag) {
        mi.setOnAction(event -> {
                    l.setText("");
                    tagsOfBookList.remove(tag);
                }
        );
    }

}
