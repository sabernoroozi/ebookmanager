package UI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Author;
import model.Manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AuthorController {
    Manager manager = new Manager();
    List<HBox> hBoxes = new ArrayList<>();
    List<Author> authors = new ArrayList<>();

    @FXML
    private AnchorPane rootPane;

    @FXML
    private VBox vbox;

    @FXML
    private Button newButton;

    @FXML
    private Button addButton;

    @FXML
    void addAuthor(MouseEvent event) {
        manager.addAuthorList(getAuthors(hBoxes));
        Stage stage = (Stage)addButton.getScene().getWindow();
        stage.close();

//        try {
//            AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("AuthorForm.fxml"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    public List<Author> getAuthors(List<HBox> hBoxes){
        for(HBox hBox: hBoxes){
            String name = ((TextField)hBox.getChildren().get(0)).getText();
            authors.add(new Author(null, name));
        }
        return authors;
    }

    @FXML
    void newAuthor(MouseEvent event) {
            handleNewAuthor();
    }

    public VBox handleNewAuthor(){
        HBox hBox = new HBox(10);
        hBox.setPrefWidth(361);
        hBox.setPrefHeight(35);
        TextField name = new TextField();
        name.setPromptText("name");
        name.setPrefWidth(245);
        name.setPrefHeight(25);
        name.setPadding(new Insets(5, 0, 5, 5));
        hBox.getChildren().addAll(name);
        hBoxes.add(hBox);
        vbox.getChildren().add(hBox);
        return vbox;
    }


}
