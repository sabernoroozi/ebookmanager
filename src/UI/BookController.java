package UI;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import model.Dao.TagDao;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookController {

    Stage bookController = new Stage();
    Manager manager = new Manager();
    Ebook ebook = new Ebook();
    List<Author> authors = new ArrayList<>();
    List<Comment> comments;
    List<Tag> tags =new ArrayList<>();

    static Integer help =1;
    static Integer counter=1;
    GridPane tagGridPane =new GridPane();

    File savedFile;

    @FXML
    private VBox bookVbox;

    @FXML
    private TextField title;

    @FXML
    private TextField edition;

    @FXML
    private TextField year;

    @FXML
    private TextField genre;

    @FXML
    private TextField publisher;

    @FXML
    private TextArea summary;

    @FXML
    private Button saveBook;

    @FXML
    private VBox vbox;

    @FXML
    private ComboBox<Author> authorBox;

    @FXML
    private TextField authorOrderBox;

    @FXML
    private Button newAuthor;

    @FXML
    private Button saveAuthor;

    @FXML
    private Label address;

    @FXML
    private Button browseButton;

    // *********** related to Comments tab ***********//
    @FXML
    private Button OKButton;
    @FXML
    private TextArea Content;
    @FXML
    private VBox vboxComment;

    SimpleDateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy HH:mm");
    private List<Comment> commentsList = new ArrayList<>();

    // *******************************************************************

    @FXML
    void browse(MouseEvent event) {
        try {
            Stage form = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose PDF");
            savedFile = fileChooser.showOpenDialog(form);
            address.setText(savedFile.getPath());
            if (savedFile != null) {
                System.out.println(savedFile.getParent()+ " - " + savedFile.getName().replaceAll(".pdf",""));
            }
            else {
                System.out.println("copy cancelled.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @FXML
    void showAuthors(MouseEvent event) {
        authorBox.getItems().clear();
        authorBox.getItems().addAll(manager.fetchAllAuthors());
    }

    @FXML
    void addNewAuthor(MouseEvent event) {
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("AuthorForm.fxml"));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            bookController.setScene(new Scene(root));
            bookController.show();
    }

    @FXML
    void saveNewBook(MouseEvent event) {
        addBookTable();
        ebook.setAuthors(this.authors);
        ebook.setSummary(summary.getText());
        ebook.setComments(commentsList);
        manager.add(this.ebook);
        System.out.println(title.getText());
        manager.copyPdf(savedFile.getParent()+"\\", savedFile.getName().replaceAll(".pdf", ""), title.getText());

        Stage stage = (Stage)saveBook.getScene().getWindow();
        stage.close();
    }

    @FXML
    void saveNewAuthor(MouseEvent event) {
        Integer id = authorBox.getSelectionModel().getSelectedIndex();
        Author author = manager.fetchAllAuthors().get(id);
        author.setAuthorOrder(Integer.parseInt(authorOrderBox.getText()));
        authors.add(author);
    }

    private void addBookTable(){
        ebook.setTitle(title.getText());
        ebook.setEdition(Integer.parseInt(edition.getText()));
        ebook.setYear(Integer.parseInt(year.getText()));
        ebook.setGenre(genre.getText());
        ebook.setPublisher(publisher.getText());
    }
    @FXML
    private AnchorPane TagAnchorPaneBook;

    @FXML
    private Button addTagToBook;

    @FXML
    private ComboBox tagComboBoxBook;

    @FXML
    void addTagtoBook(MouseEvent event) {
        new AddTagToBook(tagComboBoxBook, tagGridPane,tags);
    }

    @FXML
    void initializeComboBoxBook(MouseEvent event) {
        tagComboBoxBook.getItems().clear();
        TagDao tagDao =new TagDao(manager.getConnection());
        while(help ==1) {
            TagAnchorPaneBook.getChildren().add(tagGridPane);
            tagGridPane.setLayoutX(14);
            tagGridPane.setLayoutY(55);
            help++;
        }
        for(Tag tag:tagDao.getAllTags()){
            Label tagItem = new Label(tag.getLabel());
            tagItem.setTextFill(tag.getColor());
            tagComboBoxBook.getItems().add(tagItem);
        }
    }


    @FXML
    void addComment(MouseEvent event){
        System.out.println("in addComment");

        OKButton.setOnMouseClicked(e->{
            try {
                System.out.println("in OKButton");
                Comment c = new Comment(null, Content.getText(), new Date(), null);
                commentsList.add(c);

                TextField tf = new TextField(Content.getText());
                Button edit = new Button("Edit");
                Button delete = new Button("Delete");
                Label dateLbl = new Label(dateFormat.format(c.getDate()));

                Content.clear();
                HBox row = new HBox();
                row.getChildren().addAll(dateLbl, tf, edit, delete);
                vboxComment.getChildren().add(row);


                edit.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        try {

                            Stage form = new Stage();
                            FXMLLoader fx = new FXMLLoader(getClass().getResource("editCommentForm.fxml"));
                            Parent parent = fx.load();

                            //Get controller of next scene
                            editCommentFormController editCommentController = fx.getController();
                            //show current comment in next scene's textArea
                            editCommentController.setComment(c.getContent());

                            form.setScene(new Scene(parent));
                            form.showAndWait();

                            //update comment's date and content
                            c.setContent(editCommentController.getComment());
                            c.setDate(new Date());
                            tf.setText(c.getContent());


                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }

                });

                delete.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        commentsList.remove(c);
                        vboxComment.getChildren().remove(row);

                    }
                });

            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }

}
