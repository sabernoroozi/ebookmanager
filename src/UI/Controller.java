package UI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.*;
import model.Dao.SearchDao;
import model.Dao.TagDao;
import model.Dao.BookTagDao;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import java.util.ArrayList;
import java.util.List;

public class Controller implements Initializable {
    Manager manager;
    List<Ebook> inputList;
    static Integer help =1;
    static Integer counter=1;
    GridPane tagGridPane =new GridPane();
    List<Tag> tags = new ArrayList<>();
    ObservableList<Ebook> selectedBooks = FXCollections.observableArrayList();

    public Controller(){
        manager = new Manager();
        inputList = new ArrayList<>();
    }

    Stage authorWindow;
    Stage bookWindow;

    Integer tableId;

    @FXML
    private ComboBox TagComboBoxHome;

    @FXML
    private Button addTagToBookButton;

    @FXML
    private GridPane TagGridPaneHome;

    @FXML
    private AnchorPane tagAnchorPaneHome;

    @FXML
    private TableView<Ebook> tableview;

    @FXML
    private TableColumn<String, Ebook> title;

    @FXML
    private TableColumn<String, Ebook> author;

    @FXML
    private TableColumn<String, Ebook> genre;

    @FXML
    private TableColumn<String, Ebook> publisher;

    @FXML
    private TableColumn<Integer, Ebook> edition;

    @FXML
    private TableColumn<Integer, Ebook> year;

    @FXML
    private TextArea summary;

    @FXML
    private TextField titleField;

    @FXML
    private TextField authorField;

    @FXML
    private TextField publisherField;

    @FXML
    private TextField genreField;

    @FXML
    private TextField yearField;

    @FXML
    private Button addBookButton;

    @FXML
    private Button deleteBookButton;

    @FXML
    private Button addTagButton;

    @FXML
    private Button importButton;

    @FXML
    private Button exportButton;

    @FXML
    private TextField serachtextField;

    @FXML
    private Button searchButton;

    @FXML
    private ComboBox<?> filterCombo1;

    @FXML
    private ComboBox<?> filterCombo2;

    @FXML
    private Button deletefilter;

    @FXML
    private Button filterbooks;

    @FXML
    private Button editButton;

    @FXML
    private ImageView img1;

    @FXML
    private ImageView img2;

    @FXML
    private ImageView img3;

    @FXML
    private ImageView img4;

    @FXML
    private ImageView img5;

    @FXML
    private ImageView refresh;

    //************ related to comment tab ***************

    @FXML
    private Tab commentsTab;
    SimpleDateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy HH:mm");
    @FXML
    private VBox vboxComment;

    @FXML
    private Button OKButton_cmnt;

    @FXML
    private TextArea comment_area;


    //***************************************************

    /**@author: Saber
     *
     * */
    @FXML
    void addBook(MouseEvent event) {
            bookWindow = new Stage();
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("BookForm.fxml"));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            bookWindow.setScene(new Scene(root));
            bookWindow.show();
            bookWindow.setOnHiding( event1 -> {
                System.out.println("Stage is closing");
                refreshTable();
            });
    }


    /**@author: Saber
     *
     * */
    @FXML
    void deleteBook(MouseEvent event) {
        selectedBooks = tableview.getSelectionModel().getSelectedItems();
        List<Ebook> listForDelete = new ArrayList<Ebook>(selectedBooks);
        manager.delete(listForDelete);
        refreshTable();
    }

    /**@author: Mohammad
     *
     * */
    @FXML
    void exportfile(MouseEvent  event) {
        selectedBooks = tableview.getSelectionModel().getSelectedItems();
        List<Ebook> listForExport = new ArrayList<Ebook>(selectedBooks);
        try {
            Stage form = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export");
            fileChooser.setInitialFileName("Export");
            File savedFile = fileChooser.showSaveDialog(form);
            if (savedFile != null) {
                System.out.println(savedFile.getParent() + " - " + savedFile.getName());
                manager.exportBooks(listForExport,savedFile.getParent() + "\\", savedFile.getName());
            }else {
                System.out.println("Export cancelled.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**@author: Mohammad
     *
     * */
    @FXML
    void importfile(MouseEvent  event) {
        try {
            Stage form = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Import");
            File savedFile = fileChooser.showOpenDialog(form);
            if (savedFile != null) {
                System.out.println(savedFile.getParent()+ " - " + savedFile.getName().replaceAll(".zip",""));
                manager.importBokks(savedFile.getParent()+"\\", savedFile.getName().replaceAll(".zip",""));
                refreshTable();
            }
            else {
                System.out.println("Import cancelled.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @FXML
    void tableSelection(MouseEvent event) {
        if(tableview.getSelectionModel().getSelectedIndex()>=0) {
            tableview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            tableId = tableview.getSelectionModel().getSelectedIndex();
            titleField.setText(inputList.get(tableId).getTitle());
            authorField.setText(inputList.get(tableId).getAuthorsName());
            publisherField.setText(inputList.get(tableId).getPublisher());
            yearField.setText("" + inputList.get(tableId).getYear());
            genreField.setText(inputList.get(tableId).getGenre());

            TableRow<Ebook> row = new TableRow<>();
            String bookName = "";
            if ((event.getClickCount() == 2) ) {
                bookName = inputList.get(tableId).getTitle();
                System.out.println("Opening PDF file in external reader : " + bookName);
                manager.openPDF(bookName);
            }
        }else{
            System.out.println("Nothing exist in the table");
        }
        showTagsOfBook();
        handleComments(inputList.get(tableId).getId());
    }

    @FXML
    void add_comment(ActionEvent event) {

    }

    @FXML
    void add_tag(ActionEvent event) {

    }

    @FXML
    void refreshTable(MouseEvent event) {
        refreshTable();
    }

    public void refreshTable(){
        mainTable(getInputList());
        clearFields();
    }

    public ObservableList<Ebook> getInputList(){
        inputList = manager.fetchEbooks();
        ObservableList<Ebook> inputList2 = FXCollections.observableArrayList(inputList);
//        for(Ebook ebook:inputList){
//            inputList2.add(ebook);
//        }
        return inputList2;
    }

    /**@author: Saber
     *
     * */
    public void mainTable(ObservableList<Ebook> inputList) {
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        author.setCellValueFactory(new PropertyValueFactory<>("authorsName"));
        edition.setCellValueFactory(new PropertyValueFactory<>("edition"));
        year.setCellValueFactory(new PropertyValueFactory<>("year"));
        genre.setCellValueFactory(new PropertyValueFactory<>("genre"));
        publisher.setCellValueFactory(new PropertyValueFactory<>("publisher"));
        tableview.setItems(inputList);
    }

    @FXML
    void edit(MouseEvent event) {
        Ebook ebook = inputList.get(tableId);
        ebook.setTitle(titleField.getText());
        ebook.setSummary(summary.getText());
        ebook.setPublisher(publisherField.getText());
        ebook.setYear(Integer.parseInt(yearField.getText()));
        ebook.setGenre(genreField.getText());
        manager.editBook(ebook);
        addTagsafterEditPressed();
        refreshTable();
        clearFields();
    }




    /**@author: Mohammad
     *
     * */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearFields();
        refreshTable();
    }

    private void clearFields() {
        titleField.clear();
        authorField.clear();
        publisherField.clear();
        genreField.clear();
        yearField.clear();
        summary.clear();
    }


    /**@author: aalizadeh
     *
     * */
    @FXML
    void ShowMyTags(MouseEvent event) {
        ObservableList myTags = FXCollections.observableArrayList(manager.fetchTags());
        filterCombo2.setItems(myTags);
    }

    @FXML
    void ShowMyAuthors(MouseEvent event) {

        ObservableList myAuthors = FXCollections.observableArrayList(manager.fetchAuthors());
        filterCombo1.setItems(myAuthors);
    }

    @FXML
    void FilterBook(MouseEvent event) throws Exception {


        Filter dao = new Filter();
        List<String> authorList = new ArrayList<>();
        List<String > tagList = new ArrayList<>();
        List<Ebook> ebooks1 = new ArrayList<>();
        List<Ebook> ebooks2 = new ArrayList<>();



        //while (filterCombo1.getValue() != null || filterCombo2.getValue() != null) {


        String value1 = (String) filterCombo1.getValue();

        //if(value1 !=null){
        //  authorList.add(value1);
        //}
        //authorList.forEach(System.out::println);
        //System.out.println(value1);
        String value2 = (String) filterCombo2.getValue();
        //System.out.println(value2);
        if (value1 != null && value2 != null) {

            List<Integer> ids = new ArrayList<>();
            List<Integer> ids1 = new ArrayList<>();
            authorList.add(value1);
            ebooks1 = manager.fetchByAuthors(authorList);
            tagList.add(value2);
            ebooks2 = manager.fetchByTags(tagList);
            //ebooks1.addAll(ebooks2);
            for(Ebook ebook:ebooks1)
                ids.add(ebook.getId());

            for(Ebook ebook:ebooks2)
                ids1.add((ebook.getId()));

            ids.retainAll(ids1);

            if(ids.isEmpty()) {
                //tableview.getItems().clear();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("This Book does not exist!");
                alert.showAndWait();
            }else {

                for (Ebook ebook : ebooks1) {
                    if (!(ids.contains(ebook.getId())))
                        ebooks1.remove(ebook);
                }

                tableview.getItems().clear();
                ObservableList<Ebook> inputList2 = FXCollections.observableArrayList(ebooks1);
                mainTable(inputList2);

            }





        } else if (value1 != null && value2 == null) {
            authorList.add(value1);
            ebooks1 = manager.fetchByAuthors(authorList);
            tableview.getItems().clear();
            ObservableList<Ebook> inputList3 = FXCollections.observableArrayList(ebooks1);
            mainTable(inputList3);

        } else if (value2 != null && value1 == null) {
            tagList.add(value2);
            ebooks2 = manager.fetchByTags(tagList);
            tableview.getItems().clear();
            ObservableList<Ebook> inputList4 = FXCollections.observableArrayList(ebooks2);
            mainTable(inputList4);

        }


        //tableview.getItems().clear();
        //mainTable(ebooks1);
        //mainTable(ebooks2);
        //if(!ebooks1.isEmpty()) {
        //ebooks1.forEach(System.out::println);
        //}//
        //if(!ebooks2.isEmpty()) {
        //ebooks2.forEach(System.out::println);
        //}//

    }

    @FXML
    void deleteFilter(MouseEvent event) {
        String myValue1 = (String) filterCombo1.getValue();
        String myValue2 = (String) filterCombo2.getValue();
        //String myValue = "";

        if(myValue1 != null || myValue2 != null){
            filterCombo1.setValue(null);
            filterCombo2.setValue(null);
        }
        refreshTable();
    }

    /**addTag method, initializeTagComboBoxHome method,
     * addTagToBookHome method, addTagsafterEditPressed method,
     * showTagsofBook method
     *
     * author Houra Keshavarz
     * 18/08/2019
     */
    public void addTagsafterEditPressed(){
        AddTagToBook.tagsOfBookList.forEach(System.out::println);
        BookTagDao bookTagDao =new BookTagDao(new Manager().getConnection());
        for(Tag tag:AddTagToBook.tagsOfBookList){
            bookTagDao.addTagBook(tag, inputList.get(tableId).getId());
        }
    }

    @FXML
    void addTag(MouseEvent event) {
        new AddTag();
    }

    @FXML
    void initializeTagComboBoxHome(MouseEvent event) {
        TagComboBoxHome.getItems().clear();
        TagDao tagDao =new TagDao(manager.getConnection());
        for(Tag tag:tagDao.getAllTags()){
            Label tagItem = new Label(tag.getLabel());
            tagItem.setTextFill(tag.getColor());
            TagComboBoxHome.getItems().add(tagItem);
        }
    }
    public  void showTagsOfBook(){
        //tagGridPane.getChildren().clear();
        new ShowTagsofBook(tagGridPane,inputList.get(tableId).getId(), counter,tags);

        if(!tagAnchorPaneHome.getChildren().contains(tagGridPane)) {
            tagAnchorPaneHome.getChildren().add(tagGridPane);
        }

    }
    @FXML
    void addTagtoBookHome(MouseEvent event) {
        new AddTagToBook(TagComboBoxHome, tagGridPane,tags);
    }

    @FXML
    void searchClicked(MouseEvent event) {
        if(!serachtextField.getText().trim().isEmpty()){

            ObservableList<Ebook> searchBooks = FXCollections.observableArrayList(
                    new SearchDao(manager.getConnection()).search(serachtextField.getText()));
            tableview.getItems().clear();
            mainTable(searchBooks);
        }
    }


    public void handleComments(Integer bookId){

        // show comments on tab selection in MainForm
        List<Comment> comments = inputList.get(tableId).getComments();
        System.out.println("book_id: "+inputList.get(tableId).getId());
        System.out.println("size: "+inputList.get(tableId).getComments().size());
        System.out.println();

        vboxComment.getChildren().clear();
        OKButton_cmnt.setOnMouseClicked(e-> {
            try{

                Comment c = new Comment(null, comment_area.getText(), new Date(), inputList.get(tableId).getId());
//                    comments.add(c);

                TextField tf = new TextField(comment_area.getText());
                Button edit = new Button("Edit");
                Button delete = new Button("Delete");
                Label dateLbl = new Label(dateFormat.format(c.getDate()));

                comment_area.clear();
                HBox row = new HBox();
                row.getChildren().addAll(dateLbl, tf, edit, delete);
                vboxComment.getChildren().add(row);
                inputList.get(tableId).getComments().add(c);
                manager.addComment(inputList.get(tableId).getId(), c);

            }catch (Exception ex){
                ex.printStackTrace();
            }
        });


        for (Comment comment: inputList.get(tableId).getComments()) {
            TextField tf = new TextField(comment.getContent());
            Button edit = new Button("Edit");
            Button delete = new Button("Delete");
            Label dateLbl = new Label(dateFormat.format(comment.getDate()));

            HBox row = new HBox();
            row.getChildren().addAll(dateLbl, tf, edit, delete);
            vboxComment.getChildren().add(row);

            edit.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    try {

                        Stage form = new Stage();
                        FXMLLoader fx = new FXMLLoader(getClass().getResource("editCommentForm.fxml"));
                        Parent parent = fx.load();

                        //Get controller of next scene
                        editCommentFormController editCommentController = fx.getController();
                        //show current comment in next scene's textArea
                        editCommentController.setComment(comment.getContent());

                        form.setScene(new Scene(parent));
                        form.showAndWait();

                        //update comment's date and content
                        comment.setContent(editCommentController.getComment());
                        comment.setDate(new Date());
                        tf.setText(comment.getContent());

                        manager.editComment(comment);


                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                }

            });

            delete.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
//                    comments.remove(comment);
                    manager.deleteComment(comment.getBookId());
                    inputList.get(tableId).getComments().remove(comment);
                    vboxComment.getChildren().remove(row);

                }
            });
//
        }
    }
}
