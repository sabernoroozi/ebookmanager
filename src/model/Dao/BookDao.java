package model.Dao;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import model.Author;
public class BookDao {

    public enum Action {
        ADD_BOOK,
        EDIT_BOOK,
        DELETE_BOOK,
        ADD_AUTHOR,
        EDIT_AUTHOR,
        DELETE_AUTHOR,
        ADD_BOOK_AUTHOR,
        EDIT_BOOK_AUTHOR,
        DELETE_BOOK_AUTHOR
    }
    private Connection connection;
    private Map<Action, String> actionStatement;

    public BookDao(Connection connection) {
        this.connection = connection;
        this.actionStatement = new HashMap<>();
        preStatements();
    }

    private void preStatements(){
        String addBook = "insert into book values(null, ?, ?, ?, ?, ?, ?)";
        actionStatement.put(Action.ADD_BOOK, addBook);

        String editBook = "update book set title =?, genre=?, publisher =?, edition=?, year=?, summary=? where id = ?";
        actionStatement.put(Action.EDIT_BOOK, editBook);

        String deleteBook = "delete from book where id = ?";
        actionStatement.put(Action.DELETE_BOOK, deleteBook);

        String addAuthor = "insert into author values(null, ?)";
        actionStatement.put(Action.ADD_AUTHOR, addAuthor);

        String editAuthor = "update author set name=? , author_order = ? where id = ?";
        actionStatement.put(Action.EDIT_AUTHOR, editAuthor);

        String deleteAuthor = "delete from author where id = ?";
        actionStatement.put(Action.DELETE_AUTHOR, deleteAuthor);

        String addBookAuthor = "insert into bookAuthor values(?, ?, ?)";
        actionStatement.put(Action.ADD_BOOK_AUTHOR, addBookAuthor);

        String editBookAuthor = "update bookAuthor set author_id = ? where book_id = ? ";
        actionStatement.put(Action.EDIT_BOOK_AUTHOR, editBookAuthor);

        String deleteBookAuthor = "delete from bookAuthor where book_id = ? and author_id = ?";
        actionStatement.put(Action.DELETE_BOOK_AUTHOR, deleteBookAuthor);
    }

    public int addBook(String title, String genre, String publisher, int edition, int year, String summary){
        int out = 0;
        try {
            PreparedStatement addBook = connection.prepareStatement(actionStatement.get(Action.ADD_BOOK), Statement.RETURN_GENERATED_KEYS);
            addBook.setString(1, title);
            addBook.setString(2, genre);
            addBook.setString(3, publisher);
            addBook.setInt(4, edition);
            addBook.setInt(5, year);
            addBook.setString(6, summary);
            addBook.executeUpdate();
            ResultSet bookResult = addBook.getGeneratedKeys();
            while (bookResult.next()){
                out = bookResult.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public void editBook(int id, String title, String genre, String publisher, int edition, int year, String summary){
        try {
            PreparedStatement editBook = connection.prepareStatement(actionStatement.get(Action.EDIT_BOOK));
            editBook.setString(1, title);
            editBook.setString(2, genre);
            editBook.setString(3, publisher);
            editBook.setInt(4, edition);
            editBook.setInt(5, year);
            editBook.setString(6, summary);
            editBook.setInt(7, id);
            editBook.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int deleteBook(Integer bookId){
        int out = 0;
        try {
            PreparedStatement deleteBook = connection.prepareStatement(actionStatement.get(Action.DELETE_BOOK));
            deleteBook.setInt(1, bookId);
            out = deleteBook.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public int addAuthor(Author author){
        int out =0;
        try {
            PreparedStatement addAuthor = connection.prepareStatement(actionStatement.get(Action.ADD_AUTHOR), Statement.RETURN_GENERATED_KEYS);
            addAuthor.setString(1, author.getName());
            addAuthor.executeUpdate();
            ResultSet authorResult = addAuthor.getGeneratedKeys();
            while (authorResult.next()){
                out = authorResult.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public void editAuthor(Author author){
        try {
            PreparedStatement editAuthor = connection.prepareStatement(actionStatement.get(Action.EDIT_AUTHOR));
            editAuthor.setString(1, author.getName());
            editAuthor.setInt(2, author.getAuthorOrder());
            editAuthor.setInt(3, author.getId());
            editAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int deleteAuthor(int authorId){
        int out = 0;
        try {
            PreparedStatement deleteAuthor = connection.prepareStatement(actionStatement.get(Action.DELETE_AUTHOR));
            deleteAuthor.setInt(1, authorId);
            deleteAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public void addBookAuthor(int bookId, int authorId, int authorOrder){
        try {
            PreparedStatement addBookAuthor = connection.prepareStatement(actionStatement.get(Action.ADD_BOOK_AUTHOR));
            addBookAuthor.setInt(1, bookId);
            addBookAuthor.setInt(2, authorId);
            addBookAuthor.setInt(3, authorOrder);
            addBookAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


//    public void editBookAuthor(int bookId){
//        try {
//            PreparedStatement editBookAuthor = connection.prepareStatement(actionStatement.get(Action.EDIT_BOOK_AUTHOR));
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    public int deleteBookAuthor(int bookId, int authorId){
        int out =0;
        try {
            PreparedStatement deleteBookAuthor = connection.prepareStatement(actionStatement.get(Action.DELETE_BOOK_AUTHOR));
            deleteBookAuthor.setInt(1, bookId);
            deleteBookAuthor.setInt(2, authorId);
            out = deleteBookAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }




}
