package model.Dao;


import model.Ebook;
import model.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



/**
 * The TagBookDao class manages all should be done about "database table TagBook" which is
 * the junction table between tables Book and Tag.
 * It manages adding a tag to a book , deleting a tag from a book, and check if one Tag exists for a book
 *
 * @author  Houra Keshavarz
 * @version 1.0
 * @since   2019-08-10
 */


public class BookTagDao {
    private Connection connection;
    private PreparedStatement addStatement;
    private PreparedStatement deleteStatement;
    private PreparedStatement existanceStatement;
    private TagDao tagDao;
    private BookDao bookDao;
    private QueryDao queryDao;
    public BookTagDao(Connection connection) {
        this.connection = connection;
        tagDao = new TagDao(connection);
        bookDao = new BookDao(this.connection);
        queryDao = new QueryDao(this.connection);
        prepareStatement();
    }
    private void prepareStatement() {
        try {
            addStatement = connection.prepareStatement("insert into booktag values (?,?)");
            deleteStatement = connection.prepareStatement("delete from booktag where (tag_id, book_id)=(?,?) ");
            existanceStatement = connection.prepareStatement("SELECT * from booktag where (tag_id, book_id)=(?,?)");
        }catch (SQLException ex){
            ex.getMessage();
        }
    }
    public void addTagBook(Tag tag, Integer bookid) {
        try {

            if(!(exist(tag, bookid))) {
                System.out.println(tagDao.getTag(tag).getId());
                addStatement.setInt(2, tagDao.getTag(tag).getId());
                addStatement.setInt(1, bookid);
                addStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteTagBook(Tag tag, Integer bookid){
        try {
            deleteStatement.setInt(1, tagDao.getTag(tag).getId());
            deleteStatement.setInt(2, bookid);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Boolean exist(Tag tag, Integer bookid){
        try {
            existanceStatement.setInt(1, tagDao.getTag(tag).getId());
            existanceStatement.setInt(2,bookid);
            ResultSet resultSet=existanceStatement.executeQuery();
            if(resultSet.next()){
                return true;

            }} catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
