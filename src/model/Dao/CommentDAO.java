package model.Dao;



import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import model.Comment;


public class CommentDAO {

    static enum CommentStatement {
        ADD,
        EDIT,
        DELETE,
        FIND_BY_BOOKID,
        FIND_BY_BOOKID_CID,
        FIND_ALL
    }

    private Connection connection;

    private Map<CommentStatement, PreparedStatement> statements = new HashMap<>();

    public CommentDAO(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException();
        }
        this.connection = connection;

        prepareStatements();
    }

    private void prepareStatements() {
        try {

            statements.put(CommentDAO.CommentStatement.FIND_ALL, this.connection.prepareStatement(
                    "select * from comments"));
            statements.put(CommentStatement.FIND_BY_BOOKID_CID, this.connection.prepareStatement(
                    "select * from comments where book_id = ? and id = ?"
            ));
            statements.put(CommentDAO.CommentStatement.ADD, this.connection.prepareStatement(
                    "insert into comments values (null, ?, ?, ?)"
            ));
            statements.put(CommentStatement.DELETE, this.connection.prepareStatement(
                    "delete from comments where book_id = ?"
            ));
            statements.put(CommentDAO.CommentStatement.EDIT, this.connection.prepareStatement(
                    "update comments set content = ?, date = ? where book_id = ? and id = ?"
            ));

            statements.put(CommentStatement.FIND_BY_BOOKID, this.connection.prepareStatement(
                    "select * from comments where book_id = ?"));

        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public int deleteComment(Integer bookId) {
        PreparedStatement deleteCommentStatement = statements.get(CommentDAO.CommentStatement.DELETE);
        try {
            deleteCommentStatement.setInt(1, bookId);


            return deleteCommentStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public int addComment(Integer bookId, Comment comment) {
        PreparedStatement addCommentStatement = statements.get(CommentDAO.CommentStatement.ADD);
        try {
//            addCommentStatement.setInt(1, comment.getId());
            addCommentStatement.setString(1, comment.getContent());
            java.util.Date utilDate = comment.getDate();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            addCommentStatement.setDate(2, sqlDate);
//            addCommentStatement.setInt(3, comment.getBookId());
            addCommentStatement.setInt(3, bookId);


            return addCommentStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public int editComment(Comment comment) {
        PreparedStatement editCommentStatement = statements.get(CommentDAO.CommentStatement.EDIT);
        try {
//            editCommentStatement.setInt(1, comment.getId());
            editCommentStatement.setString(1, comment.getContent());
            editCommentStatement.setDate(2, (Date) comment.getDate());
            editCommentStatement.setInt(3, comment.getBookId());
            editCommentStatement.setInt(4, comment.getId());

            return editCommentStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Comment> findCommentsByBookId(Integer id){
        PreparedStatement findCommentsByBookIdStatement = statements.get(CommentStatement.FIND_BY_BOOKID);
        final List<Comment> comments = new ArrayList<>();
        try {
            findCommentsByBookIdStatement.setInt(1, id);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return fetchComments(comments, findCommentsByBookIdStatement);
    }

    public List<Comment> findCommentsByBookId_cId(Integer bookId, Integer cId) {
        PreparedStatement findCommentsByBookIdStatement = statements.get(CommentStatement.FIND_BY_BOOKID_CID);
        final List<Comment> comments = new ArrayList<>();

        try {
            findCommentsByBookIdStatement.setInt(1, bookId);
            findCommentsByBookIdStatement.setInt(2, cId);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return fetchComments(comments, findCommentsByBookIdStatement);
    }


    private List<Comment> fetchComments(List<Comment> comments, PreparedStatement findById) {

        try (ResultSet resultSet = findById.executeQuery();) {
            while (resultSet.next()) {
                Comment comment = new Comment(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getDate(3),
                        resultSet.getInt(4));
                comments.add(comment);

            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return comments;
    }


    public List<Comment> findAllComments() {
        final List<Comment> comments = new ArrayList<>();
        return fetchComments(comments, statements.get(CommentDAO.CommentStatement.FIND_ALL));
    }

}

