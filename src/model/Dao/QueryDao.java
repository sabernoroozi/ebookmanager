package model.Dao;

import model.Author;
import model.Ebook;
import model.Table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class QueryDao {

    public enum Query {
        FETCH_ALL_BOOKS,
        FETCH_ALL_AUTHORS,
        FIND_AUTHOR_BY_ID,
        FIND_AUTHOR_BY_NAME,
        FILTER_BOOK_AUTHOR_BY_AUTHOR_ID,
        OUT_LIST
    }
    private Connection connetion;
    private Map<Query, String> queryStatement;

    public QueryDao(Connection connetion) {
        this.connetion = connetion;
        queryStatement = new HashMap<>();
        queries();
    }

    private void queries(){
        String findAuthorByName = "select * from author where name = ?";
        queryStatement.put(Query.FIND_AUTHOR_BY_NAME, findAuthorByName);

        String filterBookAuthorByAuthorId = "select count(*) from bookAuthor where author_id = ?";
        queryStatement.put(Query.FILTER_BOOK_AUTHOR_BY_AUTHOR_ID, filterBookAuthorByAuthorId);

        String outPutList = "select book.id, title, genre, publisher, edition,year, summary,group_concat(name) as name from book\n" +
                "    inner join bookauthor b on book.id = b.book_id\n" +
                "    inner join author a on b.author_id = a.id\n" +
                "group by book.id;";
        //, title, genre, publisher, edition, year, summary
        queryStatement.put(Query.OUT_LIST, outPutList);

        String fetchAllAuthors = "select * from author";
        queryStatement.put(Query.FETCH_ALL_AUTHORS, fetchAllAuthors);

        String fetchAllBooks = "select * from book";
        queryStatement.put(Query.FETCH_ALL_BOOKS, fetchAllBooks);

        String findAuthorById = "select  author.id, author.name, bookauthor.author_order from bookauthor\n" +
                "inner join author on bookauthor.author_id= author.id\n" +
                "where bookauthor.book_id = ?";
        queryStatement.put(Query.FIND_AUTHOR_BY_ID, findAuthorById);


    }

    public Integer isAuthorValid(String name){
        Integer out = null;
        try {
            PreparedStatement isAuthorValid = connetion.prepareStatement(queryStatement.get(Query.FIND_AUTHOR_BY_NAME));
            isAuthorValid.setString(1, name);
            isAuthorValid.execute();
            ResultSet resultSet = isAuthorValid.getResultSet();
            while(resultSet.next()){
                out = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public Integer bookAuthorCount(Integer authorId){
        Integer out =0;
        try {
            PreparedStatement bookAuthorCount = connetion.prepareStatement(queryStatement.get(Query.FILTER_BOOK_AUTHOR_BY_AUTHOR_ID));
            bookAuthorCount.setInt(1, authorId);
            bookAuthorCount.execute();
            ResultSet resultSet = bookAuthorCount.getResultSet();
            while(resultSet.next()){
                out = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public Ebook getBook(Ebook book){
        try {
            PreparedStatement getBookStatement = connetion.prepareStatement("select * from book where" +
                    "(title, genre, publisher, edition, book.year)=(?,?,?,?,?)");
            getBookStatement.setString(1, book.getTitle());
            getBookStatement.setString(2, book.getGenre());
            getBookStatement.setString(3, book.getPublisher());
            getBookStatement.setInt(4, book.getEdition());
            getBookStatement.setInt(5, book.getYear());

            ResultSet resultSet = getBookStatement.executeQuery();
            if (resultSet.next()) {
                System.out.println(resultSet+"res");
                return new Ebook(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getInt(6),
                        resultSet.getString(7)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Author> fetchAllAuthors(){
        List<Author> authors = new ArrayList<>();
        try {
            PreparedStatement fetchAllAuthors = connetion.prepareStatement(queryStatement.get(Query.FETCH_ALL_AUTHORS));
            fetchAllAuthors.execute();
            ResultSet resultSet = fetchAllAuthors.getResultSet();
            while(resultSet.next()){
                Integer id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                authors.add(new Author(id, name, 0));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }

    public List<Table> outList(){
        List<Table> tables = new ArrayList<>();
        try {
            PreparedStatement outList = connetion.prepareStatement(queryStatement.get(Query.OUT_LIST));
            outList.execute();
            ResultSet resultSet = outList.getResultSet();
            while (resultSet.next()){
                tables.add(new Table(resultSet.getString(2),
                        resultSet.getString(8),
                        resultSet.getInt(5),
                        resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tables;
    }

    public List<Ebook> fetchAllBooks(){
        Ebook book;
        List<Ebook> books = new ArrayList<>();
        try {
            PreparedStatement fetchAllBooks = connetion.prepareStatement(queryStatement.get(Query.FETCH_ALL_BOOKS));
            fetchAllBooks.execute();
            ResultSet resultSet = fetchAllBooks.getResultSet();
            while (resultSet.next()){
                book = new Ebook(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getInt(6),
                        resultSet.getString(7));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    public List<Author> findAuthorsById(Integer bookId){
        List<Author> authors = new ArrayList<>();
        try {
            PreparedStatement findAuthorsById = connetion.prepareStatement(queryStatement.get(Query.FIND_AUTHOR_BY_ID));
            findAuthorsById.setInt(1, bookId);
            findAuthorsById.execute();
            ResultSet resultSet = findAuthorsById.getResultSet();
            while (resultSet.next()){
                authors.add(new Author(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getInt(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }
}
