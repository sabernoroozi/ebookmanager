package model.Dao;

import model.Ebook;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SearchDao {
    private Connection connection;
    private PreparedStatement searchStatement;
    public SearchDao(Connection connection) {
        this.connection = connection;
        try {
            searchStatement = this.connection.prepareStatement("select * from book where title like ?");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<Ebook> search(String title){
        List<Ebook> searchedBooks = new ArrayList<>();
        try {
            searchStatement.setString(1,"%"+title+"%");
            ResultSet resultSet = searchStatement.executeQuery();
            while(resultSet.next()){
                Ebook book = new Ebook(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getInt(6),
                        resultSet.getString(7));
                searchedBooks.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  searchedBooks;
    }
}
