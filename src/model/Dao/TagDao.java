package model.Dao;


import javafx.scene.paint.Color;

import java.awt.print.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Tag;
import model.Ebook;
/**
 * The TagDao class manages all should be done about "database table Tag" including
 * adding a Tag, deleting a Tag, getting a Tag, getting all Tags, and editing one Tag
 *
 * @author  Houra Keshavarz
 * @version 1.0
 * @since   2019-08-10
 */
public class TagDao {
    private Connection connection;
    private PreparedStatement addTagStatement;
    private PreparedStatement deleteTagStatement;
    private PreparedStatement deleteTagBookStatement;
    private PreparedStatement editTagStatement;
    private PreparedStatement getAllTagsStatement;
    private PreparedStatement getTagStatement;
    private PreparedStatement findByTag;
    private  PreparedStatement findTagsbyBook;
    QueryDao queryDao;
    public TagDao(Connection connection){
        this.connection = connection;
        queryDao = new QueryDao(this.connection);
        prepareStatement();
    }

    private void prepareStatement() {
        try {
            addTagStatement = connection.prepareStatement(
                    "insert into tag value (null, ?,?)");
            deleteTagStatement = connection.prepareStatement("delete from tag where id =?");
            deleteTagBookStatement = connection.prepareStatement("delete from booktag where tag_id =?");
            editTagStatement = connection.prepareStatement("update tag  set label = ?, color =? where" +
                    " id=?");
            getAllTagsStatement = connection.prepareStatement("select * from tag");
            getTagStatement = connection.prepareStatement("SELECT * from tag where (label,color) = (?,?)");
            findByTag = connection.prepareStatement("select * from book b join booktag tb on b.id = tb.book_id " +
                    "join Tag t  on tb.tag_id=t.id where t.id = ?" );
            findTagsbyBook = connection.prepareStatement("select * from tag t join booktag tb on t.id = tb.tag_id" +
                    " join book b on b.id=tb.book_id where b.id =?");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addTag(Tag tag){
        try {
            if(getTag(tag)==null) {
                addTagStatement.setString(1, tag.getLabel());
                addTagStatement.setString(2, tag.getColor().toString());
                addTagStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteTag(Tag tag){
        try {
            if (!(getTag(tag) == null)) {
                deleteTagStatement.setInt(1, getTag(tag).getId());
                deleteTagBookStatement.setInt(1, getTag(tag).getId());
                deleteTagStatement.executeUpdate();
                deleteTagBookStatement.executeUpdate();
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void editTag(Tag oldTag, Tag newTag){
        try {
            if(!(getTag(oldTag)==null)) {
                editTagStatement.setString(1, newTag.getLabel());
                editTagStatement.setString(2, newTag.getColor().toString());
                editTagStatement.setInt(3, getTag(oldTag).getId());
                editTagStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<Tag> getAllTags(){
        List<Tag> tags = new ArrayList<>();
        try {
            ResultSet resultSet = getAllTagsStatement.executeQuery();
            while (resultSet.next()){
                Tag tag = new Tag(resultSet.getInt(1),resultSet.getString(2),
                        Color.valueOf(resultSet.getString(3)));
                tags.add(tag);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tags;
    }
    public Tag getTag(Tag tag){
        try {
            getTagStatement.setString(1, tag.getLabel());
            getTagStatement.setString(2, tag.getColor().toString());
            ResultSet resultSet = getTagStatement.executeQuery();
            if (resultSet.next()) {
                return new Tag(resultSet.getInt(1),
                        resultSet.getString(2),
                        Color.valueOf(resultSet.getString(3)));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**filterBooksByTag returns Books which have some special Tag
     *
     * @author  Maryam Aalizadeh
     * @version 1.0
     * @since   2019-08-11
     */
    public List<Ebook> filterBooksByTag(Tag tag){
        List<Ebook> books = new ArrayList<>();
        try {
            if (!(getTag(tag) == null)) {
                findByTag.setInt(1, getTag(tag).getId());
                ResultSet resultSet = findByTag.executeQuery();
                while (resultSet.next()) {
                    Ebook book = new Ebook(resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getInt(5),
                            resultSet.getInt(6),
                            resultSet.getString(7));
                    books.add(book);
                }
            }
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return books;
    }
    public List<Tag> findTagsbyBook(Integer bookid){
        List<Tag> tags = new ArrayList<>();
        try {
                findTagsbyBook.setInt(1, bookid);
                ResultSet resultSet =findTagsbyBook.executeQuery();
                while(resultSet.next()) {
                    Tag tag = new Tag(resultSet.getInt(1), resultSet.getString(2),
                            Color.valueOf(resultSet.getString(3)));
                    tags.add(tag);
                }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return tags;
    }
}
