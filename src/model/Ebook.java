package model;


import java.util.ArrayList;
import java.util.List;

public class Ebook {
    private Integer id;
    private String title;
    private List<Author> authors;
    private String genre;
    private String publisher;
    private Integer edition;
    private Integer year;
    private String summary;
    private List<Comment> comments;
    private List<Tag> tags;
    private String authorsName = "";



    public Ebook(){}
    public Ebook(Integer id, String title, String genre, String publisher, Integer edition, Integer year, String summary) {
        this.id = id;
        this.title = title;
        this.authors = new ArrayList<>();
        this.genre = genre;
        this.publisher = publisher;
        this.edition = edition;
        this.year = year;
        this.summary = summary;
        this.comments = new ArrayList<>();
        this.tags = new ArrayList<>();
    }

    public void setAuthorsName(String authorsName) {
        this.authorsName = authorsName;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(Author author) {
        this.authors.add(author);
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setEdition(Integer edition) {
        this.edition = edition;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setComments(Comment comment) {
        this.comments.add(comment);
    }

    public void setTags(Tag tag) {
        this.tags.add(tag);
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public String getGenre() {
        return genre;
    }

    public String getPublisher() {
        return publisher;
    }

    public Integer getEdition() {
        return edition;
    }

    public Integer getYear() {
        return year;
    }

    public String getSummary() {
        return summary;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public String getAuthorsName(){
        String name = "";
        for(int i=0; i<authors.size(); i++){
            if ((authors.size()<=1) || (authors.size()==i+1)){
                name += authors.get(i).getName();
            }else {
                name += authors.get(i).getName() + ", ";
            }
        }
        //authorsName = authors.get(0).getName();
        setAuthorsName(name);
        return authorsName;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", authors=" + authors +
                ", genre='" + genre + '\'' +
                ", publisher='" + publisher + '\'' +
                ", edition=" + edition +
                ", year=" + year +
                ", summary='" + summary + '\'' +
                ", comments=" + comments +
                ", tags=" + tags +
                '}';
    }

}
