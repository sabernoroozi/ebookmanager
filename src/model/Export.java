package model;


/**
 * 2019/08/11 sunday
 * 09:00 AM to 13:00 PM
 * @author : Mohammad Ibrahimkhah
 * pair : Ghazal Mohammadi
 * task : Export data to csv
 *
 *
 *
 * 2019/08/12 monday
 * 10:00 AM to 11:00 AM
 * Mohammad Ibrahimkhah
 * task : adding copy + some refinements
 *        change date from String to Date
 *        tested for functionality
 *
 *
 * 2019/08/14 wednesday
 * 09:30 AM to 11:00 AM
 * Mohammad Ibrahimkhah
 * task : adding zip and unzip
 */



import com.opencsv.CSVWriter;
import model.Ebook;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Export {

    public void exportBooks(List<Ebook> bookList, String bookDirectory, String exportFolderAddress, String exportName){
        List<Path> srcFiles = new ArrayList<>();
        Path pathBook = Paths.get(exportFolderAddress + exportName + "_Book.CSV");
        Path pathAuthor = Paths.get(exportFolderAddress + exportName + "_Author.CSV");
        Path pathComment = Paths.get(exportFolderAddress + exportName + "_Comment.CSV");
        Path pathTag = Paths.get(exportFolderAddress + exportName + "_Tag.CSV");
        srcFiles.add(pathBook); srcFiles.add(pathAuthor); srcFiles.add(pathComment); srcFiles.add(pathTag);
        for (Ebook ebook:bookList){
            Path pathPDF = Paths.get(exportFolderAddress+exportName+" - "+ebook.getTitle()+".pdf");
            srcFiles.add(pathPDF);
        }

        List<String[]> bookData = getBookData(bookList, bookDirectory, exportFolderAddress, exportName);
        List<String[]> authorData = getAuthorData(bookList);
        List<String[]> commentData = getCommentData(bookList);
        List<String[]> tagData = getTagData(bookList);

        csvWriter(bookData,pathBook);
        csvWriter(authorData, pathAuthor);
        csvWriter(commentData, pathComment);
        csvWriter(tagData,pathTag);

        Zip.putInZip(srcFiles, exportFolderAddress+exportName);
        deleteTempFiles(srcFiles);
    }

    private List<String[]> getBookData(List<Ebook> bookList, String bookDirectory, String exportFolderAddress, String exportName) {
        List<String[]> bookData = new ArrayList<>();
        for (Ebook book:bookList) {
            String [] temp = new String[7];
            temp[0] = String.valueOf(book.getId());
            temp[1] = book.getTitle();
            temp[2] = book.getGenre();
            temp[3] = book.getPublisher();
            temp[4] = String.valueOf(book.getEdition());
            temp[5] = String.valueOf(book.getYear());
            temp[6] = book.getSummary();
            bookData.add(temp);
            FileCopy.copy(bookDirectory, exportFolderAddress, book.getTitle());
            File oldfile =new File(exportFolderAddress+book.getTitle()+".pdf");
            File newfile =new File(exportFolderAddress+exportName+" - "+book.getTitle()+".pdf");
            oldfile.renameTo(newfile);
        }
        return bookData;
    }

    private List<String[]> getAuthorData(List<Ebook> bookList) {
        List<String[]> authorData = new ArrayList<>();
        for (Ebook book:bookList) {
            Integer bookID = book.getId();
            for(Author author:book.getAuthors()) {
                String[] temp = new String[3];
                temp[0] = String.valueOf(bookID);
                temp[1] = author.getName();
                temp[2] = String.valueOf(author.getAuthorOrder());
                authorData.add(temp);
            }
        }
        return authorData;
    }

    private List<String[]> getCommentData(List<Ebook> bookList) {
        List<String[]> commentData = new ArrayList<>();
        for (Ebook book:bookList) {
            Integer bookID = book.getId();
            for(Comment comment:book.getComments()) {
                String[] temp = new String[3];
                temp[0] = String.valueOf(bookID);
                temp[1] = String.valueOf(comment.getDate());
                temp[2] = comment.getContent();
                commentData.add(temp);
            }
        }
        return commentData;
    }

    private List<String[]> getTagData(List<Ebook> bookList) {
        List<String[]> tagData = new ArrayList<>();
        for (Ebook book:bookList) {
            Integer bookID = book.getId();
            for(Tag tag:book.getTags()) {
                String[] temp = new String[3];
                temp[0] = String.valueOf(bookID);
                temp[1] = tag.getLabel();
                temp[2] = tag.getColor().toString();
                tagData.add(temp);
            }
        }
        return tagData;
    }

    private void csvWriter(List<String[]> stringArray, Path path){
        try
        {
            CSVWriter writer = new CSVWriter(new FileWriter(path.toString()));
            writer.writeAll(stringArray);
            writer.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void deleteTempFiles(List<Path> srcFiles) {
        for (Path path:srcFiles) {
            File file = new File(path.toString());
            file.delete();
        }
    }
}
