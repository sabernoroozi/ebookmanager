package model;

import model.Author;
import model.Ebook;

import java.sql.*;
import java.util.*;

public class Filter {


    private Connection connection = null;
    private PreparedStatement filterByAuthor = null;
    private PreparedStatement filterByTag1 = null;
    private PreparedStatement filterByTag = null;
    private PreparedStatement getAllTags = null;
    private PreparedStatement getAllAuthors = null;

    public List<Ebook> filterByAuthor(List<String> authors) throws Exception{
        try {

            //Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost/bookdatabase?"
                            + "user=root&password=saber882301051");

            filterByAuthor = connection.prepareStatement("select distinct b.id,title, genre, publisher, edition,year, summary,group_concat(name) as name from book b join bookauthor c on b.id = c.book_id " +
                    "join author a  on a.id=c.author_id where name = ? group by book_id order by author_id");

            filterByTag1 = connection.prepareStatement("select distinct group_concat(name) as name from author a join bookauthor c on c.author_id = a.id join" +
                    " book b on c.book_id = b.id where title = ?");

            List<Ebook> books = new ArrayList<>();
            for(String temp:authors){
                String author = temp;
                filterByAuthor.setString(1,author );
                ResultSet resultSet = filterByAuthor.executeQuery();
                while (resultSet.next()) {
                    Ebook book = new Ebook(resultSet.getInt(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getString(4), resultSet.getInt(5),
                            resultSet.getInt(6),resultSet.getString(7));
                    String book_name = resultSet.getString(2);
                    filterByTag1.setString(1,book_name );
                    ResultSet resultSet1 = filterByTag1.executeQuery();
                    resultSet1.next();
                    book.setAuthor(new Author(resultSet1.getString(1)));
                    books.add(book);

                }
            }


            return books;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Ebook> filterByTag(List<String> tags){
        try {

            //Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost/bookdatabase?"
                            + "user=root&password=saber882301051");

            filterByTag1 = connection.prepareStatement("select distinct group_concat(name) as name from author a join bookauthor c on c.author_id = a.id join" +
                    " book b on c.book_id = b.id where title = ?");

            filterByTag = connection.prepareStatement("select distinct b.id,title, genre, publisher, edition,year, summary from book b join booktag tb on b.id = tb.book_id " +
                    "join Tag t  on tb.tag_id=t.id where label = ? group by book_id" );

            List<Ebook> books = new ArrayList<>();
            for(String temp:tags){
                String author = temp;
                filterByTag.setString(1,author );
                ResultSet resultSet = filterByTag.executeQuery();
                while (resultSet.next()) {
                    Ebook book = new Ebook(resultSet.getInt(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getString(4), resultSet.getInt(5),
                            resultSet.getInt(6),resultSet.getString(7));
                    String book_name = resultSet.getString(2);
                    filterByTag1.setString(1,book_name );
                    ResultSet resultSet1 = filterByTag1.executeQuery();
                    resultSet1.next();
                    book.setAuthor(new Author(resultSet1.getString(1)));
                    books.add(book);

                }
            }


            return books;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public List<String> retrieveAllAuthors(){

        try {

            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost/bookdatabase?"
                            + "user=root&password=saber882301051");

            getAllAuthors = connection.prepareStatement("select name from author");

            List<String> authorlist = new ArrayList<String>();

            ResultSet resultSet = getAllAuthors.executeQuery();
            while (resultSet.next()) {
                String tag = resultSet.getString(1);
                authorlist.add(tag);
            }
            return authorlist;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<String> retrieveAllTags(){

        try {

            connection = DriverManager
                    .getConnection("jdbc:mysql://localhost/bookdatabase?"
                            + "user=root&password=saber882301051");

            getAllTags = connection.prepareStatement("select label from tag");

            List<String> taglist = new ArrayList<String>();

            ResultSet resultSet = getAllTags.executeQuery();
            while (resultSet.next()) {
                String tag = resultSet.getString(1);
                taglist.add(tag);
            }
            return taglist;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }





}

