package model;

import javafx.scene.shape.Path;
import model.Dao.BookDao;
import model.Dao.CommentDAO;
import model.Dao.QueryDao;
import model.Dao.TagDao;

import java.awt.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Manager {
    private final String url = "jdbc:mysql://localhost:3306/bookdatabase";
    private final String userName = "root";
    private final String password = "saber882301051";

    private Connection connection;
    private BookDao bookDao;
    private QueryDao queryDao;
    private CommentDAO commentDAO;
    private TagDao tagDao;
    private Export exportBooks;
    private Import importBooks;
    private String bookDirectory;

    public Manager(){
        try {
            this.connection = DriverManager.getConnection(url, userName, password);
            bookDao = new BookDao(connection);
            queryDao = new QueryDao(connection);
            commentDAO = new CommentDAO(connection);
            tagDao = new TagDao(connection);
            exportBooks = new Export();
            importBooks = new Import();
            createBookDirectory();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createBookDirectory() {
        if (Files.notExists(Paths.get(System.getProperty("user.home") + "\\Book Directory"))) {
            new File(System.getProperty("user.home") + "\\Book Directory").mkdirs();
            System.out.print("Book Directory created: ");
        }
//        }else {
//            System.out.print("Book Directory is already exists: ");
//        }
        bookDirectory = System.getProperty("user.home").toString() + "\\Book Directory\\";
        System.out.print(bookDirectory+"\n");
    }

    public void importBokks(String exportFolderAddress, String exportName) {
        try {
            List<Ebook> bookList = importBooks.importBooks(bookDirectory, exportFolderAddress, exportName);
            addForImport(bookList);
            System.out.println("Import Successful");
        }catch (IOException ex){
            ex.printStackTrace();
        }catch (ParseException ex){
            ex.printStackTrace();
        }
    }

    public void copyPdf(String sourceAddress, String fileName, String titleName){
        FileCopy.copy(sourceAddress, bookDirectory, fileName);
        File oldFile = new File(bookDirectory + fileName + ".pdf");
        File newFile = new File(bookDirectory + titleName + ".pdf");
        oldFile.renameTo(newFile);
    }

    public void exportBooks(List<Ebook> listForExport,String exportFolderAddress, String exportName) {
//        List<Ebook> books = fetchEbooks();
        exportBooks.exportBooks(listForExport,bookDirectory,exportFolderAddress,exportName);
        System.out.println("Export Successful");
    }

    public void add(Ebook ebook){
        int bookId = addBook(ebook);
        List<Author> authors = ebook.getAuthors();
        for(Author author: authors){
            addBookAuthor(bookId, author.getId(), author.getAuthorOrder());
        }
        for(Comment comment:ebook.getComments()){
            addComment(bookId, comment);
        }
    }

    public void addForImport(Ebook ebook){
        int bookId = addBook(ebook);
        List<Author> authors = ebook.getAuthors();
        for(Author author: authors){
            int authorId = addAuthor(author);
            addBookAuthor(bookId, authorId, author.getAuthorOrder());
        }
    }
    public void addForImport(List<Ebook> ebooks){
        for(Ebook ebook:ebooks){
            addForImport(ebook);
        }
    }
    public void delete(Ebook ebook){
        for(Author author: ebook.getAuthors()){
            int count = queryDao.bookAuthorCount(author.getId());
            if(count != 1){
                deleteBookAuthor(ebook.getId(), author.getId());
                deleteBook(ebook.getId());
            }else{
                deleteAuthor(author.getId());
                deleteBookAuthor(ebook.getId(), author.getId());
                deleteBook(ebook.getId());
            }
        }
        File file = new File(bookDirectory + "\\" + ebook.getTitle() + ".pdf");
        file.delete();

//        deleteBook(ebook.getId());
    }
    public void delete(List<Ebook> ebooks) {
        for(Ebook ebook:ebooks){
            delete(ebook);
        }
    }
    public int addBook(Ebook ebook){
        return bookDao.addBook(ebook.getTitle(), ebook.getGenre(), ebook.getPublisher(), ebook.getEdition(), ebook.getYear(), ebook.getSummary());
    }

    public void addBook(List<Ebook> ebooks){
        for (Ebook ebook:ebooks){
            add(ebook);
        }
    }

    public void editBook(Ebook ebook){
        bookDao.editBook(ebook.getId(), ebook.getTitle(), ebook.getGenre(), ebook.getPublisher(), ebook.getEdition(), ebook.getYear(), ebook.getSummary());
    }

    public int deleteBook(int id){
        return bookDao.deleteBook(id);
    }

    public int addAuthor(Author author){
        return bookDao.addAuthor(author);
    }

    public void updteAuthor(Author author){
        bookDao.editAuthor(author);
    }

    public void deleteAuthor(int authorId){
        bookDao.deleteAuthor(authorId);
    }

    public void addAuthorList(List<Author> list){
        for(Author author:list){
            bookDao.addAuthor(author);
        }
    }

    public void addBookAuthor(int bookId, int authorId, int authorOrder){
        bookDao.addBookAuthor(bookId, authorId, authorOrder);
    }

    public void deleteBookAuthor(int bookId, int authorId){
        bookDao.deleteBookAuthor(bookId, authorId);
    }

    public List<Author> fetchAllAuthors(){
        return queryDao.fetchAllAuthors();
    }

//    public List<Table> getTables(){
//        return queryDao.outList();
//    }

    public List<Ebook> fetchEbooks(){
        List<Ebook> books = queryDao.fetchAllBooks();
        for(Ebook ebook : books){
            ebook.setAuthors(queryDao.findAuthorsById(ebook.getId()));
            ebook.setComments(commentDAO.findCommentsByBookId(ebook.getId()));
            ebook.setTags(tagDao.findTagsbyBook(ebook.getId()));
        }
        return books;
    }

    public Connection getConnection() {
        return connection;
    }

    public void openPDF(String bookName) {
        File file = new File(bookDirectory+"\\"+bookName+".pdf");
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**@author: Maryam Aalizadeh
     *
     * */


    public List<String> fetchAuthors(){
        Filter dao = new Filter();
        List<String> authors = new ArrayList<String>();
        authors = dao.retrieveAllAuthors();
        return authors;
    }

    public List<String> fetchTags(){
        Filter dao = new Filter();
        List<String> tags = new ArrayList<String>();
        tags = dao.retrieveAllTags();
        return tags;
    }

    public List<Ebook> fetchByTags(List<String> tags){
        Filter dao = new Filter();
        List<Ebook> ebooks = new ArrayList<>();
        ebooks = dao.filterByTag(tags);
        return ebooks;

    }

    public List<Ebook> fetchByAuthors(List<String> authors) throws Exception {
        Filter dao = new Filter();
        List<Ebook> ebooks = new ArrayList<>();
        ebooks = dao.filterByAuthor(authors);
        return ebooks;

    }

    /**@author: Fahimeh Mahmoudi
     *
     * */
    public void addComment(Integer bookId, Comment comment){
        commentDAO.addComment(bookId, comment);
    }

    public void deleteComment(Integer bookId){
        commentDAO.deleteComment(bookId);
    }

    public void editComment(Comment comment){
        commentDAO.editComment(comment);
    }
}
